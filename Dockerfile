FROM centos:7
RUN curl -L https://dl.yarnpkg.com/rpm/yarn.repo -o /etc/yum.repos.d/yarn.repo
RUN curl --silent --location https://rpm.nodesource.com/setup_14.x | bash -
WORKDIR /code
RUN yum install -y nodejs yarn
RUN npm install
RUN npm run build
EXPOSE 8080
CMD npm start
